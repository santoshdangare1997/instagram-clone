<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
    // return view('welcom-screen');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('post/likePost', [App\Http\Controllers\HomeController::class, 'likePost'])->name('likePost');
Route::get('/profile/{username}', [App\Http\Controllers\HomeController::class, 'getUserProfile'])->name('getUserProfile');
Route::post('post/savepost', [App\Http\Controllers\HomeController::class, 'savePost'])->name('savepost');

Route::get('user/get-saved-post', [App\Http\Controllers\HomeController::class, 'getUserSavedPosts'])->name('get-saved-post');
Route::post('user/get-user-posts', [App\Http\Controllers\HomeController::class, 'getUserPosts'])->name('get-user-posts');





