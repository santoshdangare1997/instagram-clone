<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => '1',
            'post_caption' => 'this is demo post',
            'latitude' => '56.95',
            'longitude' => '94.95',
            'post_media' => 'post-1.jpg',
            'status' => 1,
            'latitude' => bcrypt('secret'),
        ]);
    }
}
