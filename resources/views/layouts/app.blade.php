<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="this is instagram clone app">
    <meta name="description" content="this application is created by the santosh dangare.">
    <title> Instagram Clone</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.6.min.js') }}" ></script>
    
    <!-- favicon -->
    <link rel="shortcut icon" href="{{url('/assets/images/insta-favico-32px.png')}}" type="image/x-icon" sizes="32x32">
    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
    <div id="app">
    @if(Auth::check())
    <!-- start header part -->
        <section class="header-section">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{-- config('app.name', 'Laravel') --}}
                        <img class="img-fluid insta-logo" src="{{url('/assets/images/instagram-logo-name.png.png')}}  " alt="instagram-logo-name">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                                    </li>
                                @endif
                            @else
                            <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fas fa-home"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-facebook-messenger"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="far fa-compass"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="far fa-heart"></i></a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{-- Auth::user()->username --}}   <!-- Display username of user -->
                                        <img class="user-profile-icon" src="{{url('/assets/images/user-profile-images/sd.jpg')}} " alt="profile icon">
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#"><i class="far fa-user-circle"></i> Profile</a>
                                        <a class="dropdown-item" href="#"><i class="far fa-bookmark"></i> Saved</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> Settings</a>
                                        <hr>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>

                                       

                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        <!-- end header part -->
    @endif
        <main class="py-4">
        <div class="loader">
    <img src="/assets/images/loader.gif" alt="">
</div>
            @yield('content')
        </main>
    </div>
    <!-- footer start -->
    <section class=" footer">
        <div class="footer-content">
            <ul class="footer-list ">
                <li class="footer-item"><a class="nav-link" href="#" >About</a></li>
                <li class="footer-item"><a class="nav-link" href="#" >About</a></li>
                <li class="footer-item"><a class="nav-link" href="#" >About</a></li>
                <li class="footer-item"><a class="nav-link" href="#" >About</a></li>
                <li class="footer-item"><a class="nav-link" href="#" >About</a></li>
            </ul>
        </div>
    </section>
    <!-- footer end -->
    
</body>
<script src="{{ asset('js/custom.js')}}"></script>
</html>
