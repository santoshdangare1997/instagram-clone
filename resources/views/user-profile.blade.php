@extends('layouts.app')

@section('content')

<section class="section-user-profile">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3 pl-5">
                <div class="text-center ">
                <img class="img-fluid user-profile"  src="{{url('/assets/images/user-profile-images/'.$arrUserProfileData[0]->profile_pic)}}  " alt="instagram-logo-name">
                </div>
            </div>
            <div class="col-md-6">
                <?php  #echo '<pre>';print_r($arrUserProfileData[0]->profile_pic);  ?>
                <div class="user-details">
                    <span class="loggedin-username">{{ Auth::user()->username }}</span>
                   <button class="btn btn-light edit-profile-btn ml-3 mb-3">Edit Profile</button>
                   <i class="fa fa-cog ml-1 mb-4 setting-btn"></i> 
                </div>
                <div>
                    <span class="mr-4"><span class="user-stats">55</span> posts</span>
                    <span class="mr-4 ml-3"><span class="user-stats">550</span> Followers</span>
                    <span><span class="user-stats">260</span> Following</span>
                </div>
                <div class="mt-3">
                    <p class="fullname mb-0"> {{ $arrUserProfileData[0]->first_name }} {{ $arrUserProfileData[0]->last_name }}</p>
                    <p class="account-type mt-0">Personal Blog</p>
                    <p class="account-desc mt-0">I code, what's your super power? </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="user-tab">
    <div class="container">
        <hr>
        <div class="row justify-content-center">
            <div class="profile-tab-section">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" id="user-posts-btn">
                        <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true"><i class="fas fa-border-all"></i> Posts</a>
                    </li>
                    <li class="nav-item igtv-btn">
                        <a class="nav-link" id="igtv-tab" data-toggle="tab" href="#igtv" role="tab" aria-controls="igtv" aria-selected="false"><i class="fas fa-tv"></i> IGTV</a>
                    </li>
                    <li class="nav-item" id="saved-posts-btn">
                        <a class="nav-link" id="saved-tab" data-toggle="tab" href="#saved" role="tab" aria-controls="saved" aria-selected="false"><i class="far fa-bookmark"></i> Saved</a>
                    </li>
                     <li class="nav-item tagged-btn">
                        <a class="nav-link" id="tagged-tab" data-toggle="tab" href="#tagged" role="tab" aria-controls="tagged" aria-selected="false"><i class="far fa-id-badge"></i> Tagged</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <!-- start posts tab -->
                    <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                        <div class="container">
                            <div class="row" id="append-post">
                                <div class="col-md-4 col-sm-12">
                                    <div class="user-posts-block p-3">
                                        <img class="img-fluid users-single-post" src="{{url('/assets/images/posts/post-1.jpg')}}" alt="post-media">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="user-posts-block p-3">
                                        <img class="img-fluid users-single-post " src="{{url('/assets/images/posts/post-1.jpg')}}" alt="post-media">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- end posts tab -->

                    <!-- start igtv  tab -->
                    <div class="tab-pane fade" id="igtv" role="tabpanel" aria-labelledby="igtv-tab">igtv srese...</div>
                    <!-- end igtv tab -->
                    
                    <!-- start saved tab -->
                    <div class="tab-pane fade" id="saved" role="tabpanel" aria-labelledby="saved-tab">saved...</div>
                    <!-- end saved tab -->

                    <!-- start tagged tab -->
                    <div class="tab-pane fade" id="tagged" role="tabpanel" aria-labelledby="tagged-tab">tagged...</div>
                    <!-- end tagged tab -->

                </div>
            </div>
        </div>
    </div>
</section>

<script type="application/javascript">
    $(document).ready(function(){
        
        $("#user-posts-btn").unbind("click").click(function(){
            $('.loader').show();
            $.ajax('/user/get-user-posts', {
                type: 'POST',
                data: { '_token': "{{ csrf_token() }}" },
                success: function (data, status, xhr) {
                    console.log(data.response);
                    if(data.status == 1){   
                       
                        var adddata = [];
                        $.each(data.response, function(key,val) {
                             adddata += `<div class="col-md-4 col-sm-12">
                                            <div class="user-posts-block p-3">
                                                <img class="img-fluid users-single-post" src="/assets/images/posts/${val.post_media}" alt="${val.postId}">
                                            </div>
                                        </div>`;
                            $('.loader').fadeOut();
                            $('#append-post').html('');
                            $('#append-post').html(adddata);
                        });  
                        
                    }else{
                        console.log('No data available..');
                    }          
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    console.log('errorMessage');
                }
            });
        });


        $("#saved-posts-btn").unbind("click").click(function(){
            // console.log('saved');return false;
            $.ajax('/user/get-saved-post', {
                type: 'GET',
                data: { '_token': "{{ csrf_token() }}" },
                success: function (data, status, xhr) {
                    console.log(data.response);return false;
                    if(data.status == 1){
                        // console.log('fdf');
                        // append-post
                        
                    }else{
                        console.log('No data available..');
                    }

                    // $.each(data.programs, function(key,val) {
                    //     // console.log(val);
                    //     $('#InId').append("Name: " +val.name+" & Price: "+val.price+"<br/>");
                    // });
                    
                    // $('.append-posts .users-single-post').attr('src', data.response);
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    console.log(errorMessage);
                }
            });
        });

    // $(document).on('click','.igtv-btn',function(){
    //     console.log('igtv btn clicked');
    // });


    // $(document).on('click','.tagged-btn',function(){
    //     console.log('tagged btn clicked');
    // });

     });
   
</script>

@endsection

