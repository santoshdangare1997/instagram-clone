<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Instagram Mobile Login Page In Bootstrap 4 - W3hubs.com</title>
        <meta charset="utf-8">
        <meta name="description" content="instagram clone">
        <meta name="author" content="santosh dangare">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            /*W3ubs.com - Download Free Responsive Website Layout Templates designed on HTML5 CSS3,
Bootstrap which are 100% Mobile friendly. w3Hubs all Layouts are responsive cross 
browser supported, best quality world class designs.*/
@import url('https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Montserrat:300i,400,400i,500,600,700');
body{
	padding: 0;
	margin: 0;
	font-family: 'Dancing Script', cursive;
font-family: 'Montserrat', sans-serif;

}
h1{
	text-align: center;
	padding: 10px;
	font-family: 'Dancing Script', cursive;
	font-size: 110px;
}

.theme-name{
    font-size: 70px;}
form{
	width: 50%;
	margin: 0 auto;
	display: block;
	padding: 20px;
	
}
.btn {
	display: block;
	width: 100%;
	font-size: 16px;
	font-weight: 700;
	letter-spacing: 1px;
}
h4{
	text-align: center;
	padding-top: 20px;
	padding-bottom: 10px;
	text-transform: capitalize;
	font-size: 18px;
	color: #AAB7B8;
	line-height: 2;
	margin-top: 5px;
}
h4:before,h4:after{
	height: 1.5px;
    display: inline-block;
    width: 100px;
    background: #E5E7E9;
    border-right: 1px white;
    content: '';
    margin-left: 10px;
    padding: 1px;
    margin-right: 10px;

}
.form-control{
	background: #F4F6F7;
	font-size: 18px;
	height: 50px;
}
p{
	
	text-align: center;
}

p a{
	text-align: center;
	color: #2980B9 !important;
	font-weight: 500;
	text-decoration: none;
} 
p a:hover{
	color: #2E86C1;
	text-decoration: none;
}
.fa{
	padding-right: 10px;
	font-size: 26px !important;
	color: #1F618D;
	
	line-height: 2;
}
#w3hubs .box{
	
              border-bottom: 1px solid #D0D3D4 !important;
              border-top: 1px solid #D0D3D4 !important;
              height: 35px;
              background: #ECF0F1;

}
.box p{
	text-align: center;
	padding: 50px;
	font-size: 30px;

}

@media(max-width: 992px){
	.form-control{
		font-size: 14px;
	}
}
@media(max-width: 768px){
	form{
		width: 90%;
	}
}
@media(max-width:550px ){
	.box p {
    
    font-size: 24px;
}
}
@media(max-width: 462px){
		.box p {
    
    font-size: 20px;
}
h1{
	font-size: 90px;
}

}
@media(max-width: 390px){
	.form-control {
    font-size: 12px;
}
h1{
	font-size: 80px;
}
.box p {
    font-size: 16px;
}
}
        </style>
    </head>
    <body>
        <section id="w3hubs">
            <div class="container">
                <div class="mt-5"><h1 class="theme-name">Instagram</h1></div>
                {{$user = Auth::user();}}
                <form action="/login" method="POST">
                {{ csrf_field() }}
                    <!-- if there are login errors, show them here -->
                    <p>
                        {{ $errors->first('email') }}
                        {{ $errors->first('password') }}
                    </p>

                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control"  value="{{ old('email') }}" placeholder="username, or email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Log In</button>
                </form>
                <h4>OR</h4>
                <p><i class="fa fa-facebook-square"></i><a href="#">Log In Facebook</a></p>
                <p><a href="#">Forget Password</a></p>
            </div>
            <div class="box">
                <p>Don't have account? <a href="#">Sign up</a></p>
            </div>
            
        
        </section>
    </body>
</html>
