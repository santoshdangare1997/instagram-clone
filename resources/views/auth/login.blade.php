@extends('layouts.app')

@section('content')
<section class="login-page">
    <div class="container">            
        <div class="card">
            <div class="text-center mt-4 mb-1">
                <img class="img-fluid insta-name-logo" src="{{url('/assets/images/instagram-logo-name.png.png')}}  " alt="imgff" >
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}" id="login-form">
                    @csrf

                    <div class="form-group ">
                        <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                        <div class="">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror custom-input" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email"> 

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group ">
                        <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->

                        <div class="">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror custom-input" name="password" required autocomplete="current-password" placeholder="Password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{--  <!--<div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">Remember Me</label>
                            </div>
                        </div>
                    </div> -->  --}}

                    <div class="form-group">
                        <div class="">
                            <button type="submit" class="btn btn-primary login-btn">Log in</button>
                        </div>
                    </div>
                    <!-- <div><span class="solid-left-line"></span> <span>  OR </span> <span class="solid-right-line"></span></div> -->
                    <span class="divider line one-line" contenteditable>OR</span>

                    <div class="text-center fb-section">
                        <i class="fab fa-facebook-square"></i><a href="#" class="fb-login-btn">Log in with facebook</a>
                    </div>
                    @if (Route::has('password.request'))
                        <div class="text-center">
                            <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>        
                    @endif
                </form>
            </div>
        </div>
        <div class="card mt-1 sign-up-card">
            <div class="card-body text-center">
                <label>don't have account?<label><a class="btn btn-link sign-up-btn" href="{{ route('register') }}">Sign up</a>
            </div>
        </div>
    </div>
</section>
@endsection
