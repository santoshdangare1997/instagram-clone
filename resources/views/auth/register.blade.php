@extends('layouts.app')

@section('content')
    <section class="register-page">
        <div class="container">        
            <div class="card">
                <div class="text-center mt-4 mb-1">
                    <img class="img-fluid insta-name-logo" src="{{url('/assets/images/instagram-logo-name.png.png')}}  " alt="instagram-logo-name">
                    <h2 class="sign-up-heading">Sign up to see photos and videos from your friends.</h2>
                    <a href="#" class="btn btn-primary" type="button"><i class="fab fa-facebook-square"></i> <span class=""></span>Log in with Facebook</a>
                    <div class="divider line one-line" contenteditable="false">OR</div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group ">
                            <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->
                            <div class="">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror custom-input" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <!-- <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label> -->
                            <div class="">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror custom-input" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Full name">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <!-- username  -->
                        <div class="form-group ">
                            <!-- <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('username') }}</label> -->
                            <div class="">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror custom-input" name="username" value="{{ old('username') }}" required autocomplete="name" autofocus placeholder="username">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <!-- password -->
                        <div class="form-group ">
                            <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> -->
                            <div class="">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror custom-input" name="password" required autocomplete="new-password" placeholder="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <!-- <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label> -->
                            <div class="">
                                <input id="password-confirm" type="password" class="form-control custom-input" name="password_confirmation" required autocomplete="new-password" placeholder="confirm password">
                            </div>
                        </div>
                        <div class="form-group  mb-0">
                            <div class="">
                                <button type="submit" class="btn btn-primary" id="sign-up-btn">Sign up</button>
                            </div>
                        </div>
                        <p class="term-policy">By signing up, you agree to our <a href="#" tabindex="0">Terms</a> , <a href="#" tabindex="0">Data Policy</a> and <a href="#" tabindex="0">Cookies Policy</a> .</p>
                    </form>
                </div>
            </div>
            <div class="card mt-1 login-card">
            <div class="card-body text-center">
                <label>Have an account?<label><a class="btn btn-link sign-up-btn" href="{{ route('login') }}">Log in</a>
            </div>
        </div>
        </div>
    </section>
@endsection
