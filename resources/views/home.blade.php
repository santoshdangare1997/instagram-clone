@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
        <div class="col-md-8">
            <!-- START story-section -->
            <div class="story-section">
                <div><p>story section</p></div>
            </div>
<?php          //echo '<pre>'; print_r($postData); ?>
            <!-- END story-section -->
            <!--START Post section -->
            @foreach($user_post_data as $posts)
            <div class="post-section mb-3">
                    <div class="post-head">
                        <div class="head-content">
                            <div class="post-user-pic">
                                <img class="img-fluid" src="{{asset('/assets/images/user-profile-images/'. $posts->profile_pic)}}  " alt="instagram-logo-name">
                            </div>
                            <div class="name-section">
                                <a class="post-username" href="#">{{ $posts->username }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="post-body">
                        <div class="user-posts">
                            <img class="" src="{{ asset('assets/images/posts/' . $posts->post_media) }}" alt="post-media">   
                        </div>
                    </div>
                    <div class="post-desc">
                        <div class="post-stats"> 
                            <a data-id="{{ $posts->postId }}"  data-str="{{ $posts->post_caption }}" onClick="likePost(this)"  ><i class="{{ $posts->nlikeId > 0 ? 'fas fa-heart' : 'far fa-heart' }}" style="color:{{ $posts->nlikeId > 0 ? 'red' : '' }};"></i></a> 
                            <a class="post-comment-btn"><i class="far fa-comment"></i></a>
                            <a class="post-share-btn"><i class="far fa-paper-plane"></i></a>
                            <a data-id="{{ $posts->postId }}" class="post-share-btn text-right" onClick="savePost(this);"><i class=" {{ $posts->nSavedId > 0 ? 'fas fa-bookmark' : 'far fa-bookmark' }} "></i></a>
                            <!-- <i class="fas fa-bookmark"></i> -->
                        </div>
                    </div>
                </div>
                @endforeach
            <!--END Post section -->
        </div>

        <!--START Right sidebar -->
        <div class="col-lg-4 col-xl-4 col-md-4 mt-2">
            <div class="right-sidebar">
                <div class="logged-user-section">
                    <div class="logged-in-user-profile"> 
                        <img class="img-fluid" src="{{url('/assets/images/user-profile-images/sd.jpg')}}  " alt="instagram-logo-name">
                    </div>
                    <div class="middle-section">
                        <a href="#" class="logged-in-username"> {{ Auth::user()->username }} </a>    <!-- Display username of user -->
                        <p class="logged-in-name"> {{ Auth::user()->name }}</p>
                    </div>
                    <div class="third-setion ml-5"><a href="profile/{{ Auth::user()->username}}" class="link-btn"> View</a></div>
                    <hr>
                </div>
                <div class="suggestion-heading"> <span>Suggestions For You</span> <span>See All</span></div>
                <div class="suggested-user-section">
                    <div class="user-profile"> 
                        <img class="img-fluid" src="{{url('/assets/images/user-profile-images/sb.jpg')}}  " alt="instagram-logo-name">
                    </div>
                    <div class="middle-section">
                        <a href="#" class="username"> {{ __('shagar_bhujang') }} </a>    <!-- Display username of user -->
                        <p class="fullname"> {{ __('shagar bhujang') }}</p>
                    </div>
                    <div class="third-setion ml-5"><a href="#" class="link-btn"> Follow </a></div>
                </div>
                <div class="suggested-user-section">
                    <div class="user-profile"> 
                        <img class="img-fluid" src="{{url('/assets/images/user-profile-images/sb.jpg')}}  " alt="instagram-logo-name">
                    </div>
                    <div class="middle-section">
                        <a href="#" class="username"> {{ __('shagar_bhujang') }} </a>    <!-- Display username of user -->
                        <p class="fullname"> {{ __('shagar bhujang') }}</p>
                    </div>
                    <div class="third-setion ml-5"><a href="#" class="link-btn"> Follow </a></div>
                </div>
                <div class="suggested-user-section">
                    <div class="user-profile"> 
                        <img class="img-fluid" src="{{url('/assets/images/user-profile-images/sb.jpg')}}  " alt="instagram-logo-name">
                    </div>
                    <div class="middle-section">
                        <a href="#" class="username"> {{ __('shagar_bhujang') }} </a>    <!-- Display username of user -->
                        <p class="fullname"> {{ __('shagar bhujang') }}</p>
                    </div>
                    <div class="third-setion ml-5"><a href="#" class="link-btn"> Follow </a></div>
                </div>  
            </div>
        </div>
        <!--END Right sidebar -->
        <!-- <i class="fas fa-heart"></i> -->
</div>
<?php #echo '<pre>'; print_r($user_post_data) ?>

<script type="application/javascript">

    // $(document).ready(function(){

    //     $(".like_post_btn").click(function(){
    //         // alert('ddd');
    //         console.log(this);
    //         // console.log($(this).attr("data-str"));
    //         // console.log($(this).attr("data-id"));

    //         // $.ajax('/post/likePost', {
    //         //     type: 'POST',  // http method
    //         //     data: { myData: 'This is my data.' },  // data to submit
    //         //     success: function (data, status, xhr) {
    //         //         $('p').append('status: ' + status + ', data: ' + data);
    //         //     },
    //         //     error: function (jqXhr, textStatus, errorMessage) {
    //         //             $('p').append('Error' + errorMessage);
    //         //     }
    //         // });

    //     });
    // });

    function likePost(el){
        var postId = $(el).attr('data-id');
        $.ajax('/post/likePost', {
                type: 'POST',
                data: { 'postId':postId,
                        '_token': "{{ csrf_token() }}" },
                success: function (data, status, xhr) {
                    if(xhr.status == 200 && data.status == 'liked'){
                        console.log('post liked sucessfully');
                        if( $(el).find('i').hasClass('far fa-heart')){
                            $(el).find('i').removeClass('far fa-heart');
                            $(el).find('i').addClass('fas fa-heart');
                            $(el).find('i').css('color', 'red');
                        }
                    }else if(xhr.status == 200 && data.status == 'unliked'){
                        console.log('post unliked sucessfully');

                        if($(el).find('i').hasClass('fas fa-heart')){
                                $(el).find('i').removeClass('fas fa-heart');
                                $(el).find('i').addClass('far fa-heart');
                                $(el).find('i').css('color', 'black');
                        }
                    }
                    else{
                        console.log('something went wrong');   
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    console.log(errorMessage);
                }
            });
    }

    function savePost(el){
        var postId = $(el).attr('data-id');
        if(postId != ""){
            $.ajax('/post/savepost', {
                type: 'POST',  // http method
                data: { 'postId':postId,
                        '_token': "{{ csrf_token() }}" },
                success: function (data, status, xhr) {
                    // console.log(data.response+" and   :" + data.status );
                    if(xhr.status == 200 && data.status == 'saved'){
                        console.log('post saved sucessfully');
                        if( $(el).find('i').hasClass('far fa-bookmark')){

                            $(el).find('i').removeClass('far fa-bookmark');
                            $(el).find('i').addClass('fas fa-bookmark');
                        }
                    }else if(xhr.status == 200 && data.status == 'unsaved'){
                        console.log('post unsaved sucessfully');

                        if($(el).find('i').hasClass('fas fa-bookmark')){
                                $(el).find('i').removeClass('fas fa-bookmark');
                                $(el).find('i').addClass('far fa-bookmark');
                        }
                    }
                    else{
                        console.log('something went wrong');   
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    console.log(errorMessage);
                }
            });
        }
    }
</script>

@endsection
