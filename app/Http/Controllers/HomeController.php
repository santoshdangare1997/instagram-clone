<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PostModel;
use App\Models\PostLike;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $postData = DB::table('posts')
            ->leftJoin('users', 'posts.user_id', '=', 'users.id')
            ->leftJoin('user_profile', 'posts.user_id', '=', 'user_profile.id')
            ->leftJoin('post_likes', function ($join) {
                $join->on('posts.id', '=', 'post_likes.post_id')
                    ->where('post_likes.user_id', '=', Auth::id());
            })
            ->leftJoin('saved_posts', function ($join) {
                $join->on('posts.id', '=', 'saved_posts.post_id')
                    ->where(['saved_posts.user_id' => Auth::id(), 'saved_posts.is_saved' => 1]);
            })
            ->select( 'posts.id as postId','users.*','user_profile.profile_pic','posts.post_caption','posts.post_media','posts.created_at','post_likes.id as nlikeId','saved_posts.id as nSavedId')
            ->get()->toArray();

        // $postData = DB::table('posts')
        //     ->leftJoin('post_likes' , 'posts.id' ,'=','post_likes.post_id')
        //     ->leftJoin('users', 'users.id', '=', 'post_likes.user_id')
        //     ->leftJoin('user_profile', 'user_profile.id', '=', 'users.id')
        //     ->select('posts.id as postId', 'users.*', 'posts.post_caption', 'posts.post_media', 'posts.created_at', 'user_profile.profile_pic', 'post_likes.id', DB::raw('IF (post_likes.user_id = '.auth()->user()->id.', "1", "0") as postIsLiked'))
        //     ->get()->toArray();

        // echo '<pre>'; print_r($postData);die;
            
        return view('home', [
            'user_post_data' => $postData,
            // 'arrLike' => $arrLike,
        ]);
    }

    public function likePost(Request $request){
        $response=[];
        //first check if post is liked or not
        $arrGetPostLikedData = DB::table('post_likes')
        ->where([
            'status'=> '1','post_id'=>  $request->input('postId'),'user_id'=> Auth::user()->id, 'is_liked' => 1,
            ])
        ->get();

        if(count($arrGetPostLikedData) > 0){  //update

            // $userPostLikeData = DB::table('post_likes')
            //   ->where(['post_id' => $request->input('postId'), 'user_id'=> Auth::user()->id,'status' => 1, ] )
            //   ->update(['status' => 1, 'updated_at' =>date("Y-m-d H:i:s"),  ]);

            $userPostLikeData =  DB::table('post_likes')
              ->where(['post_id' => $request->input('postId'), 'user_id'=> Auth::user()->id,'status' => 1])
              ->delete();

              $response['status'] = 'unliked';
              $response['response'] = $userPostLikeData;

        }else{ //insert 

            $userPostLikeData = DB::table('post_likes')->insert([
                'post_id' => $request->input('postId'),
                'user_id' => Auth::user()->id,
                'is_liked' => 1,
                'status' =>1,
                'created_at' =>date("Y-m-d H:i:s"),
                'updated_at' =>date("Y-m-d H:i:s"),
            ]);

            $response['status'] = 'liked';
            $response['response'] = $userPostLikeData;
        }
       
        return $response;

    }

    public function savePost(Request $request){

        $response=[];

        //first check if post is saved or not
        $arrGetPostSavedData = DB::table('saved_posts')
        ->where(['status'=> '1','post_id'=>$request->input('postId'),'user_id'=>Auth::user()->id,'is_saved'=>1,])
        ->get();

        if(count($arrGetPostSavedData) > 0){  //update

            $userPostSaveData =  DB::table('saved_posts')
                ->where(['post_id' => $request->input('postId'), 'user_id'=> Auth::user()->id,'status' => 1])
                ->delete();

            $response['status'] = 'unsaved';
            $response['response'] = $userPostSaveData;

        }else{ //insert 

            $userPostSaveData = DB::table('saved_posts')->insert([
                'post_id' => $request->input('postId'),
                'user_id' => Auth::user()->id,
                'is_saved' => 1,
                'status' =>1,
                'created_at' =>date("Y-m-d H:i:s"),
                'updated_at' =>date("Y-m-d H:i:s"),
            ]);
            
            $response['status'] = 'saved';
            $response['response'] = $userPostSaveData;

        }
       
        return $response;

    }


    public function getUserProfile(){

        $arrUserProfileData = DB::table('user_profile')
        ->where(['status'=> '1','user_id'=> Auth::user()->id])
        ->get()->toArray();

        return view('user-profile',['arrUserProfileData'=>$arrUserProfileData] );
    }


    public function getUserSavedPosts(){

        $arrGetUserSavedPosts = DB::table('posts')

        ->leftJoin('saved_posts' , 'posts.id' ,'=' , 'saved_posts.post_id')
        ->where(['posts.status'=> '1','saved_posts.status'=> '1','saved_posts.user_id'=> Auth::user()->id,'saved_posts.is_saved' => 1])
        ->select( 'posts.*','saved_posts.id as savedId' )
        ->get()->toArray();
        

        if(count($arrGetUserSavedPosts) > 0 ){
            $response['status'] = 1;
            $response['response'] = $arrGetUserSavedPosts;
        }
        
        return $response;

        
    }

    public function getUserPosts()
    {
        // echo 'waa';die;
        $userPostData = DB::table('posts')
        ->leftJoin('users', 'posts.user_id', '=', 'users.id')
        ->leftJoin('user_profile', 'posts.user_id', '=', 'user_profile.id')
        ->leftJoin('post_likes', function ($join) {
            $join->on('posts.id', '=', 'post_likes.post_id')
                ->where('post_likes.user_id', '=', Auth::id());
        })
        ->leftJoin('saved_posts', function ($join) {
            $join->on('posts.id', '=', 'saved_posts.post_id')
                ->where(['saved_posts.user_id' => Auth::id(), 'saved_posts.is_saved' => 1]);
        })
        ->select( 'posts.id as postId','users.*','user_profile.profile_pic','posts.post_caption','posts.post_media','posts.created_at','post_likes.id as nlikeId','saved_posts.id as nSavedId')
        // ->where(['users.id' => Auth::user()->id,'posts.status'=> '1','users.status'=> '1'])
        ->get()->toArray();

        if(count($userPostData) > 0 ){
            $response['status'] = 1;
            $response['response'] = $userPostData;
        }
        
        return $response;

    }

}
