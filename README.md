<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Instagram Clone

Instagram Clone is a Social Media Web Application for Users Similarto the Instagram, made by using Laravel Framework.

## About Instagram Clone

Instagram Clone is a web application  which having features similar to the Instagram where anyone can use it for study. We believe development must be an enjoyable and creative experience to be truly fulfilling. Instagram clone  takes the pain out of development by easing common tasks used in many web projects.

## Technologies Used

- **[PHP Laravel](https://laravel.com)**
- **[HTML CSS3](https://tighten.co)**
- **[Bootstrap 4](https://kirschbaumdevelopment.com)**
- **[jQuery 3](https://64robots.com)**
- **[MySQL](https://cubettech.com)**
- **[Sendgrid Mail](https://cyber-duck.co.uk)**
- **[FontAwesome 4.7](https://www.many.co.uk)**

## Installation

Clone the Repository from [GIT REPO](https://gitlab.com/santoshdangare1997/instagram-clone.git) to install the Application.

```bash
git clone https://gitlab.com/santoshdangare1997/instagram-clone.git
```

## Usage
1) Firstly Create Database name - "instagram_clone"

2) Run the following Commands.
```bash
cd instagram-clone
php artisan serve
php  artisan migrate --seed
```
3) Open .env  file and add Database name and DB user Credentials.
```php

DB_DATABASE=instagram_clone
DB_USERNAME=root
DB_PASSWORD=

```

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Santosh Dangare via [http://santoshdangare.epizy.com/](mailto:santoshdangare1997@gmail.com). All security vulnerabilities will be promptly addressed.

## Authors

[Santosh Dangare](mailto:santoshdangare1997@gmail.com)

## License
[Feel Free to use](http://santoshdangare.epizy.com/)
